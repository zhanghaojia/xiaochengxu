import { Token } from 'utils/token.js';

App({
  onLaunch: function () {
    var token = new Token();
    token.verify();
  },
  //全局变量
  globalData:{
    //1:抖音正常，0抖音相同版本，-1抖音未达到条件，3：头条，2头条lite
    adif:0
  }
  
})
