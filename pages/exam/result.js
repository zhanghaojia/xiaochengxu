var util = require('../../utils/util.js');
import { Product } from '../../utils/product-model.js';
var product = new Product();
Page({
  data: {
    actionSheetHidden: true,
    canvasStatus: false,//海报绘图标签
    posterbackgd: '/images/posterbackgd.png',
    snapImage: '',//海报产品图
    PromotionCode: '/images/xiaochengxuma.jpg',//二维码图片
    posterImageStatus: false,
    posterImage: '',//海报路径
    appName:1
  },
  onLoad: function (options) { 
    console.log("111111111111111111111");
    var that = this;
    that.setData({
          adif: getApp().globalData.adif,
        });
    //from 1:正常流程进入 //from 2:订单流程进入
    var from = options.from;
    if (from == 1){
      //根据商品id获取解析结果
      var id = options.id;//订单id
      var product_id = options.product_id;//商品id
      var score = options.score;//分值
      that.setData({
        id: id,
        product_id: product_id,
        score: score,
      });
      var OrderResultOne = wx.getStorageSync("OrderResultOne");
      console.log('onLoad'); console.log(OrderResultOne); console.log('onLoad');
      if (OrderResultOne){
        that.setData({
          OrderResultOne: OrderResultOne,//答案分析
        });
        return;
      }
      product.getAnswerData(id, product_id, score, (data) => {
        console.log(data);
        that.setData({
          OrderResultOne: OrderResultOne,//答案分析
        });
      });
    }else{
      var id = options.id;//订单id
      console.log(id);
      product.getAnswerData2(id, (data) => {
        console.log(data);
        that.setData({
          OrderResultOne: data,//答案分析
        });
      });
    }
    tt.getSystemInfo({
      success(res) {
        var appName = '';
        if(res.appName == 'Toutiao'){
          appName =   0;
        }else if(res.appName == 'Douyin'){
          appName =  1;
        }else{
          appName =  2;
        }
        that.setData({
          appName: appName
        });
      }
    })
  },
  bindtapHome:function(){
    wx.switchTab({
      url: '/pages/home/home',
    });
  },
  /**
   * 分享打开和关闭
   * 
  */
  bindchangeActionSheet: function () {
      this.setData({ actionSheetHidden: !this.data.actionSheetHidden })
  },
  
  //隐藏海报
  posterImageClose: function () {
    this.setData({ posterImageStatus: false, })
  },
  /**
   * 生成海报
  */
  bindtapCreatePoster: function() {
    var that = this;
    that.setData({ canvasStatus: true });
    var arr2 = [that.data.posterbackgd, that.data.snapImage, that.data.PromotionCode, that.nickName];
    var numtxt = that.data.OrderResultOne.snap_fakenum+1 + '人正在测试';
    wx.getImageInfo({
      src: that.data.PromotionCode,
      fail: function (res) {
        console.log('fail'); console.log(res);
        //return app.Tips({ 'title': '小程序二维码需要发布正式版后才能获取到' });
      },
      success() {
        //if (arr2[2] == '') {
          //海报二维码不存在则从新下载
        //  that.downloadFilePromotionCode(function (msgPromotionCode) {
        //    arr2[2] = msgPromotionCode;
        //    if (arr2[2] == '') return app.Tips({ title: '海报二维码生成失败！' });
        //    util.PosterCanvas(arr2, that.data.storeInfo.store_name, that.data.storeInfo.price, function (tempFilePath) {
        //      that.setData({
        //        posterImage: tempFilePath,
        //        posterImageStatus: true,
        //        canvasStatus: false,
        //        actionSheetHidden: !that.data.actionSheetHidden
        //      })
        //    });
        //  });
        //} else {
          //生成推广海报
        util.PosterCanvas(arr2, that.data.OrderResultOne.snap_name, that.data.OrderResultOne.price, numtxt, function (tempFilePath) {
            that.setData({
              posterImage: tempFilePath,
              posterImageStatus: true,
              canvasStatus: false,
              actionSheetHidden: !that.data.actionSheetHidden
            })
          });
        //}
      },
    });
  },
  //替换安全域名
  setDomain: function (url) {
    url = url ? url.toString() : '';
    //本地调试打开,生产请注销
    return url;
    if (url.indexOf("https://") > -1) return url;
    else return url.replace('http://', 'https://');
  },
  //获取海报产品图
  downloadFilesnapImage: function (imageurl) {
    console.log(imageurl);
    var that = this;
    wx.downloadFile({
      url: that.setDomain(imageurl),
      success: function (res) {
        that.setData({
          snapImage: res.tempFilePath
        })
      },
      fail: function () {
        //return app.Tips({ title: '' });
        that.setData({
          snapImage: '',
        })
      },
    });
  },
  /*
  * 保存到手机相册
  */
  bindtapSavePosterPath: function () {
    var that = this; console.log(that.data.posterImage);
    wx.getSetting({
      success(res) {
        if (!res.authSetting['scope.writePhotosAlbum']) {
          wx.authorize({
            scope: 'scope.writePhotosAlbum',
            success() {
              wx.saveImageToPhotosAlbum({
                filePath: that.data.posterImage,
                success: function (res) {
                  that.posterImageClose();
                  wx.showToast({
                    title: '保存成功',
                    icon: 'success',
                    duration: 2000
                  })
                  //app.Tips({ title: '保存成功', icon: 'success' });
                },
                fail: function (res) {
                  wx.showToast({
                    title: '保存失败',
                    duration: 2000
                  })
                  //app.Tips({ title: '保存失败' });
                }
              })
            }
          })
        } else {
          wx.saveImageToPhotosAlbum({
            filePath: that.data.posterImage,
            success: function (res) {
              that.posterImageClose();
              //app.Tips({ title: '保存成功', icon: 'success' });
              wx.showToast({
                title: '保存成功',
                icon: 'success',
                duration: 2000
              })
            },
            fail: function (res) {
              //app.Tips({ title: '保存失败' });
              wx.showToast({
                title: '保存失败',
                duration: 2000
              })
            },
          })
        }
      }
    })
  },
   onShareAppMessage (option) {
    // option.from === 'button'
    return {
      title: '点击测试【全网888万人在免费测试】',
      desc: '提供优质的智商测试、性格测试、爱情测试、趣味测试',
      path: '/pages/home/home', // ?后面的参数会在转发页面打开时传入onLoad方法
      imageUrl: '/images/logo.png', // 支持本地或远程图片，默认是小程序icon
      templateId: 'bb87ln5ge3e3cgfca8',
      success () {
        //console.log('转发发布器已调起，并不意味着用户转发成功，微头条不提供这个时机的回调');
      },
      fail () {
        //console.log('转发发布器调起失败');
      }
    }
  }
})