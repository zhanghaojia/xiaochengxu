var util = require('../../utils/util.js');
import { Banner } from '../../utils/banner-model.js';
var banner = new Banner();

import { Product } from '../../utils/product-model.js';
var product = new Product();

Page({

  onLoad: function () {
    var that = this;
    banner.getBannerData((data) => {
      console.log(data);
      that.setData({
        bannerList: data,
      });
    });

    product.getHotProductData((data) => {
      console.log(data);
      that.setData({
        hotList: data,
      });
    });
    product.getRecommendProductData((data) => {
      console.log(data);
      that.setData({
        recommodList: data,
      });
    });
    tt.getSystemInfo({
      success(res) {
        console.log("res");
        console.log(res);
        //判断是Android还是iOS
        //接入注意：目前激励视频广告仅支持抖音端，接入需判断接入宿主及宿主版本
        if(res.appName == 'Toutiao'){
          getApp().globalData.adif = 0
        }else if(res.appName == 'Douyin'){
          if(res.platform.indexOf("android") == -1 ){
            getApp().globalData.adif = util.compareVersion(res.version,'10.7');
            if(util.compareVersion(res.version,'10.7') == 0){
getApp().globalData.adif = 1;
            }
          }else{
            getApp().globalData.adif = util.compareVersion(res.version,'10.3');
            if(util.compareVersion(res.version,'10.3') == 0){
getApp().globalData.adif = 1;
            }
          }
        }else{
          getApp().globalData.adif = 2
        }
        that.setData({
          adif: getApp().globalData.adif,
        });
      }
    });
  },
  onProductItemTap: function (e) {
    var id = e.currentTarget.dataset["id"];
    wx.navigateTo({
      url: '../detail/detail?id=' + id
    })
  },

  /** 
  * 滑动切换tab 
  */
  bindChange: function (e) {
    var that = this;
    that.setData({ currentTab: e.detail.current });

  },
  /** 
   * 点击tab切换 
   */
  swichNav: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      if(e.target.dataset.current == 0){
        that.setData({
          currentTab: e.target.dataset.current,
          listItems: that.data.listItems,
          lens:3
        });
      }else if(e.target.dataset.current == 1){
        that.setData({
          currentTab: e.target.dataset.current,
          listItems: that.data.listItems2,
          lens:3
        });
      }else if(e.target.dataset.current == 2){
        that.setData({
          currentTab: e.target.dataset.current,
          listItems: that.data.listItems3,
          lens:5
        });
      }
    }
  },
  
  
  bindtapRecommend:function(){
    wx.navigateTo({
      url: '../recommend/recommend'
    })
  },
  
   onShareAppMessage (option) {
    // option.from === 'button'
    return {
      title: '点击测试【全网888万人在免费测试】',
      desc: '提供优质的智商测试、性格测试、爱情测试、趣味测试',
      path: '/pages/home/home', // ?后面的参数会在转发页面打开时传入onLoad方法
      imageUrl: '/images/logo.png', // 支持本地或远程图片，默认是小程序icon
      templateId: 'bb87ln5ge3e3cgfca8',
      success () {
        //console.log('转发发布器已调起，并不意味着用户转发成功，微头条不提供这个时机的回调');
      },
      fail () {
        //console.log('转发发布器调起失败');
      }
    }
  }

})
 
