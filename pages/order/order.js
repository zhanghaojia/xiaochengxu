import { Product } from '../../utils/product-model.js';
var product = new Product();
import { Order } from '../../utils/order-model.js';
var order = new Order();

Page({
  data: {
    currentTab: 0
  },
  onLoad: function (options) {
    var that = this;
    order.getOrderList((data) => {
      console.log(data);
      console.log('---------------------');
      that.setData({
        //allList: data.allList,
        daiList: data.daiList,
        overList: data.overList
      })
    });
  },
  //获取当前滑块的index
  bindChange: function (e) {
    const that = this;
    that.setData({
      currentTab: e.detail.current
    })
  },
  //点击切换，滑块index赋值
  checkCurrent: function (e) {
    const that = this;
    if (that.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  bindtapExam: function (e) {
    var that = this;
    var id = e.target.dataset.id;
    var product_id = e.target.dataset.product_id;
    //order.execPay(id, (statusCode) => {
      //if (statusCode == 2) {
        //开始测试
        product.getProductData(product_id, (data) => {
          console.log(data);
          that.setData({
            one: data,
          });
          wx.setStorageSync('one', data);
          wx.navigateTo({
            url: '../exam/exam?id=' + id
          })
        });
      //}
    //});
  },
  bindtapResult: function (e) {
    var that = this;
    var id = e.currentTarget.dataset.id;

    wx.navigateTo({
      url: '../exam/result?from=2&id='+id
    })
  },
   onShareAppMessage (option) {
    // option.from === 'button'
    return {
      title: '点击测试【全网888万人在免费测试】',
      desc: '提供优质的智商测试、性格测试、爱情测试、趣味测试',
      path: '/pages/home/home', // ?后面的参数会在转发页面打开时传入onLoad方法
      imageUrl: '/images/logo.png', // 支持本地或远程图片，默认是小程序icon
      templateId: 'bb87ln5ge3e3cgfca8',
      success () {
        //console.log('转发发布器已调起，并不意味着用户转发成功，微头条不提供这个时机的回调');
      },
      fail () {
        //console.log('转发发布器调起失败');
      }
    }
  }







})