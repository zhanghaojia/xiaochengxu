var zhuan = require('../../data/zhuan.js');  //引入
var gushi = require('../../data/gushi.js');  //引入
var xifang = require('../../data/xifang.js');  //引入
Page({
  data: {
    bannerArr:[{
        "id":1,
        "url":"/images/banner.png"
      },{
        "id":2,
        "url":"/images/banner.png"
      },
    ],
    currentTab: 0, 
    lens:3, 
  },

  onLoad: function () {
    var that = this;
    that.setData({
      zhuanData: zhuan.getData,
      gushiData: gushi.getData,
      xifangData: xifang.getData
    });
  },
  
  onItemTap: function (e) {
    var index = e.currentTarget.dataset["index"]; 
    var type = e.currentTarget.dataset["type"];  
console.log(type);
    if(type == 1){//精彩专题
      wx.navigateTo({
        url: '../zhuan/zhuan?index=' + index + '&type=' + type
      })
    }else if(type == 2){
      wx.navigateTo({
        url: '../article2/index?index=' + index + '&type=' + type
      })
    } else if(type == 3){
      wx.navigateTo({
        url: '../article2/index?index=' + index + '&type=' + type
      })
    } else if(type == 4){
      wx.navigateTo({
        url: '../article2/index?index=' + index + '&type=' + type
      })
    }   
    
  },
   onShareAppMessage (option) {
    // option.from === 'button'
    return {
      title: '点击测试【全网888万人在免费测试】',
      desc: '提供优质的智商测试、性格测试、爱情测试、趣味测试',
      path: '/pages/home/home', // ?后面的参数会在转发页面打开时传入onLoad方法
      imageUrl: '/images/logo.png', // 支持本地或远程图片，默认是小程序icon
      templateId: 'bb87ln5ge3e3cgfca8',
      success () {
        //console.log('转发发布器已调起，并不意味着用户转发成功，微头条不提供这个时机的回调');
      },
      fail () {
        //console.log('转发发布器调起失败');
      }
    }
  }

})