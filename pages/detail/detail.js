
import { Order } from '../../utils/order-model.js';
var order = new Order();
import { Product } from '../../utils/product-model.js';
var product = new Product();

Page({
  data: {
    tipdialog:{
      iShidden:true,
      tip:'赶紧更新最新版抖音，再来体验吧',
    }
  },
  onLoad: function (options) {
    var that = this;
    that.setData({
          adif: getApp().globalData.adif,
        });
    var id = options.id;
    product.getProductData(id, (data) => {
      console.log(data);
      that.setData({
        one: data,
      });
      wx.setStorageSync('one', data);
    });

    var userInfo = wx.getStorageSync("userInfo");
    if (!userInfo.nickName) {
      this.setData({
        iShidden: false,
      });
    } else {
      this.setData({
        iShidden: true,
      });
    }
  },
  bindtapExam:function(e){
    var that = this;
    if(getApp().globalData.adif == -1){
      that.setData({
        tipdialog:{
          iShidden:false,
          tip:'您的抖音版本太低了，赶紧去升级一下再来体验吧!',
        }
      })
      return;
    }
    var product_id = that.data.one.id;
//直接创建订单，
    var orderInfo = {
      product_id: product_id
    };
    order.createOrder(orderInfo, (data) => {
      //没有订单，创建订单生成成功
      if (data.status != -1) {
        //更新订单状态
        var id = data.order_id;
        //var order_no = data.order_no;
        //开始测试
        wx.navigateTo({
          //url: '../exam/exam?id='+id+'&order_no=' + order_no
          url: '../exam/exam?id='+id
        })
      } else {
        wx.showToast({
          title: '下单失败',
        })
      }
    });

/*
    var product_id = that.data.one.id;
    console.log("product_id:"+product_id);console.log("price:"+that.data.one.price);
    if(that.data.one.price > 0){
      console.log("platform:"+that.data.platform);
      if(that.data.platform == 2){
        tt.showToast({
          title: '苹果暂不支持，请使用安卓手机!', // 内容
          success: (res) => {}
        });
        return;
      }
      that._execPay(product_id);
    }else{
      //直接创建订单，
      var orderInfo = {
        product_id: product_id
      };
      order.createOrder(orderInfo, (data) => {
        console.log('11111111111111111111');
        console.log(data);
        //没有订单，创建订单生成成功
        if (data.status != -1) {
          //更新订单状态
          //var id = data.order_id;
          var order_no = data.order_no;
          //开始测试
          wx.navigateTo({
            url: '../exam/exam?order_no=' + order_no
          })
        } else {
          wx.showToast({
            title: '下单失败',
          })
        }
      });
    }
    */
  },

  bindtapQuery: function(e) {
      var that = this;
      //关闭弹出窗口
      that.setData({ 
        tipdialog:{
          iShidden:true,
        }
      });
    },
   onShareAppMessage (option) {
    // option.from === 'button'
    return {
      title: '点击测试【全网888万人在免费测试】',
      desc: '提供优质的智商测试、性格测试、爱情测试、趣味测试',
      path: '/pages/home/home', // ?后面的参数会在转发页面打开时传入onLoad方法
      imageUrl: '/images/logo.png', // 支持本地或远程图片，默认是小程序icon
      templateId: 'bb87ln5ge3e3cgfca8',
      success () {
        //console.log('转发发布器已调起，并不意味着用户转发成功，微头条不提供这个时机的回调');
      },
      fail () {
        //console.log('转发发布器调起失败');
      }
    }
  }
})