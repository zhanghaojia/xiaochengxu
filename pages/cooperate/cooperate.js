import { Cooperate } from '../../utils/cooperate-model.js';
var cooperate = new Cooperate();
Page({
  data: {},
  onLoad: function (options) {},
  formSubmit: function (e) {
    var that = this;
    var cooperateInfo = e.detail.value;
    if (cooperateInfo.account.length == 0) {
      wx.showToast({
        title: '请填写您的账号名称!',
        icon: 'none',
        duration: 2000,
      })
      return;
    }
    if (cooperateInfo.num.length == 0) {
      wx.showToast({
        title: '请填写您的粉丝数量!',
        icon: 'none',
        duration: 2000,
      })
      return;
    }
    if (cooperateInfo.name.length == 0) {
      wx.showToast({
        title: '请填写您的姓名!',
        icon: 'none',
        duration: 2000,
      })
      return;
    }
    
    if (cooperateInfo.contactinfo.length == 0) {
      wx.showToast({
        title: '请输入联系方式!',
        icon: 'none',
        duration: 2000,
      })
      return;
    }
    cooperate.saveCooperateInfo(cooperateInfo, (res) => {
      console.log(res);
        //提交成功
        wx.showToast({
          title: res.message,
          icon: 'none',
          duration: 2000
        })
    });

  },
  isIntNum: function(val){
    var regPos = / ^\d+$/; // 非负整数
    //var regNeg = /^\-[1-9][0-9]*$/; // 负整数
    //if(regPos.test(val) || regNeg.test(val)){
    if(regPos.test(val)){
        return true;
    }else{
        return false;
    }
    },
    onShareAppMessage (option) {
      // option.from === 'button'
      return {
      title: '点击测试【全网888万人在免费测试】',
      desc: '提供优质的智商测试、性格测试、爱情测试、趣味测试',
      path: '/pages/home/home', // ?后面的参数会在转发页面打开时传入onLoad方法
      imageUrl: '/images/logo.png', // 支持本地或远程图片，默认是小程序icon
      templateId: 'bb87ln5ge3e3cgfca8',
        success () {
          //console.log('转发发布器已调起，并不意味着用户转发成功，微头条不提供这个时机的回调');
        },
        fail () {
          //console.log('转发发布器调起失败');
        }
      }
    }
})