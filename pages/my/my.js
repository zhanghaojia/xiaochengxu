Page({
  data: {

  },
  onLoad: function (options) {
    this.setData({
          adif: getApp().globalData.adif,
        });
    var userInfo = wx.getStorageSync("userInfo");
    this.setData({
      userInfo: userInfo
    });
    if (!userInfo.nickName) {
      this.setData({
        iShidden: false,
      });
    } else {
      this.setData({
        iShidden: true,
      });
    }

  },
  /**
   * 授权回调
  */
  onLoadFun:function(e){
    var userInfo = wx.getStorageSync("userInfo");
    this.setData({
      userInfo: userInfo
    });
  },

  bindtapOrder: function (e) {
    wx.navigateTo({
      url: '../order/order'
    })
  },
  //商务合作
  bindtapCooperate: function (e) {
    wx.navigateTo({
      url: '../cooperate/cooperate'
    })
  },
  bindtapFeedback: function (e) {
    var a = this;
    wx.showModal({
      title: "温馨提示",
      content: "您将复制QQ号:" + '1429811042',
      confirmColor: "#ffa500",
      showCancel: !1,
      success: function (t) {
        wx.setClipboardData({
          data: '1429811042'
        });
      }
    });
  },

  bindGaoTap() {
    wx.navigateTo({
      url: '../my_about/about'
    })
  },
  bindAboutTap() {
    wx.navigateTo({
      url: '../my_about/about'
    })
  },
   onShareAppMessage (option) {
    // option.from === 'button'
    return {
      title: '点击测试【全网888万人在免费测试】',
      desc: '提供优质的智商测试、性格测试、爱情测试、趣味测试',
      path: '/pages/home/home', // ?后面的参数会在转发页面打开时传入onLoad方法
      imageUrl: '/images/logo.png', // 支持本地或远程图片，默认是小程序icon
      templateId: 'bb87ln5ge3e3cgfca8',
      success () {
        //console.log('转发发布器已调起，并不意味着用户转发成功，微头条不提供这个时机的回调');
      },
      fail () {
        //console.log('转发发布器调起失败');
      }
    }
  }
})