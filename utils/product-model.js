import { Base } from 'base.js';

class Product extends Base {
  constructor() {
    super();
  }

  getProductData(id, callback) {
    var param = {
      url: 'product/one?id='+id,
      sCallback: function (data) {
        callback && callback(data);
      }
    };
    this.request(param);
  }


  getHotProductData(callback) {
    var param = {
      url: 'product/hot',
      sCallback: function (data) {
        callback && callback(data);
      }
    };
    this.request(param);
  }

  getRecommendProductData(callback) {
    var param = {
      url: 'product/recommend',
      sCallback: function (data) {
        callback && callback(data);
      }
    };
    this.request(param);
  }

//id订单id， product_id 商品id
  getAnswerData(id, product_id, score, callback) {
    console.log('product/answer?id=' + id + '&product_id=' + product_id + '&score=' + score);
    var param = {
      url: 'product/answer?id=' + id + '&product_id=' + product_id + '&score=' + score,
      sCallback: function (data) {
        callback && callback(data);
      }
    };
    this.request(param);
  }
  //id订单id
  getAnswerData2(id, callback) {

    var param = {
      url: 'product/answer2?id=' + id,
      sCallback: function (data) {
        callback && callback(data);
      }
    };
    this.request(param);

  }
  

};

export { Product };