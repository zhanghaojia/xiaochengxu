import { Base } from 'base.js';

class Pay extends Base {
  constructor() {
    super();
  }

/**
 * id 订单id
 */
  execPay(id, callback) {
    var allParams = {
      url: 'pay1/pre_order',
      type: 'post',
      data: { id: id },
      sCallback: function (data) {
        callback && callback(data);
      },
      eCallback: function () {
      }
    }
    this.request(allParams);
  }

  tuneUpPay(data){
    
  }
 
}

export { Pay };