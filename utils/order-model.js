import { Base } from 'base.js';

class Order extends Base {
  constructor() {
    super();
  }

  /*创建订单*/
  createOrder(param, callback) {
    var that = this;
    var allParams = {
      url: 'order1',
      type: 'post',
      data: param,
      sCallback: function (data) {
        //that.execSetStorageSync(true);
        callback && callback(data);
      },
      eCallback: function () {
      }
    };
    this.request(allParams);
  }  
   /*修改订单 id:订单id*/
  modifyOrder(param, callback) {
    var that = this;
    var allParams = {
      url: 'order/modify',
      type: 'post',
      data: param,
      sCallback: function (data) {
        callback && callback(data);
      },
      eCallback: function () {
      }
    };
    this.request(allParams);
  }
  
  getOrderList(callback) {
    var param = {
      url: 'order1',
      sCallback: function (data) {
        callback && callback(data);
      }
    };
    this.request(param);
  }
  
  //根据订单id，查询订单
  getOne(order_no, callback) {
    var param = {
      url: 'order/one?order_no='+order_no,
      sCallback: function (data) {
        callback && callback(data);
      }
    };
    this.request(param);
  }
  

};

export { Order };