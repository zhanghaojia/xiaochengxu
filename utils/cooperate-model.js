import { Base } from 'base.js';

class Cooperate extends Base {
  constructor() {
    super();
  }

  /*创建商务合作订单*/
  saveCooperateInfo(cooperateInfo, callback) {
    var that = this;
    var allParams = {
      url: 'cooperate',
      type: 'post',
      data: {cooperateInfo:cooperateInfo},
      sCallback: function (data) {
        callback && callback(data);
      },
      eCallback: function () {
      }
    };
    this.request(allParams);
  }
  
};

export { Cooperate };