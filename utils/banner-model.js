import { Base } from 'base.js';

class Banner extends Base {
  constructor() {
    super();
  }

  getBannerData(callback) {
    var param = {
      url: 'banner',
      sCallback: function (data) {
        callback && callback(data);
      }
    };
    this.request(param);
  }

};

export { Banner };