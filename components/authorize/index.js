import { Authorize } from '../../utils/authorize-model.js';

var authorize = new Authorize();
Component({
  properties: {
    iShidden: {
      type: Boolean,
      value: true,
    }
  },
  data: {
    loading:false,
  },
  pageLifetimes: {
    hide: function () { },//关闭页面时销毁定时器
    show: function () { },//打开页面销毁定时器
  },
  detached() {},
  attached() {
    this.grantOr();
  },
  methods: {
    //授权
    bindtapTOAuthorize: function(e) {
      var that = this;
      tt.authorize({
        scope: "scope.userInfo",
        success(e) {
          // 用户同意授权用户信息
          tt.getUserInfo({
            success(res) {
              console.log(`getUserInfo 调用成功 ${res.userInfo}`);
              console.log(res.userInfo);
              console.log(`getUserInfo 调用成功 ${res.userInfo}`);
              var userInfo = res.userInfo;
              
              console.log(userInfo);
              wx.showLoading({ title: '正在登录中...' });
              if (!userInfo) {
                return;
              }
              authorize.saveUserInfo(userInfo, (res) => {
                console.log("saveUserInfo");console.log(res);console.log("saveUserInfo");
                that.setData({
                  iShidden: true,
                });
                if (res.code == 201) {
                  wx.setStorageSync('userInfo', userInfo);//成功
                }
                wx.hideLoading();
                //关闭登录弹出窗口
                that.setData({ 
                  iShidden: true
                });
                //执行回调
                that.triggerEvent('onLoadFun', userInfo);
              }); 
            },
            fail(res) {
              console.log(`getUserInfo 调用失败`);
            }
          });
          
        }
      });


      
    }, 
    /**
     * 判断是否授权
     */
    grantOr: function () {
      var that = this
      var userInfo = wx.getStorageSync('userInfo');
      console.log(userInfo);
      console.log("userInfo");
      if (true) {
        that.setData({
          iShidden: true,
        });
        //执行回调
        that.triggerEvent('onLoadFun');
      } else {
        authorize.queryUserInfo((res) => {
          if (!res.nickName) {
            that.setData({
              iShidden: false
            });
            return;
          }
          wx.setStorageSync('userInfo', res);
          that.setData({
            iShidden: true,
          });
          //执行回调
          that.triggerEvent('onLoadFun', res);
        });
      }
    }, 
  },
})